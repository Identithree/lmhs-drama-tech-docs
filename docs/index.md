---
hide:
 - navigation
 - toc
---

# Home

Welcome to the Tech Booth Documentation from the LMHS Drama Club's Tech Department.
In here, we detail everything you need to know about the booth before jumping straight into it. Think of this site as your personal lifeline in case you ever get stuck.

## Completeness

We are constantly working on and improving these docs, so there might be some holes whilst we write.