# Booth Procedures

There are various procedures for opening and closing the booth among others. These procedures are focused on rehearsals and other stuff like that. For productions, some rules are amended and are tightened. See [Production Procedure Changes](../production-changes) for more info.

## Opening up the Booth

 1. Try the door, it should be locked, however it might be unlocked depending on who was in there last and if they got someone to lock the door behind them.
    - If the door is unlocked, go to step 2
    - If the door is locked, go find either Macuga, an Administrator, or a Janitor. They will have a key. Go to Step 2
 2. Set the lights to your preferred setting. Feel free to argue with people about what setting it should go on.

### Turning on the Light Board

 1. Take the cover off the lightboard
 2. Turn on the lightboard with the power button in the top right
 3. Wait for the editing workspace to show up on the screen
 4. Switch the master control panel preset to "Off"
 5. If the show uses LED lights, make sure to turn on LED Power
    - Click "Back" twice, and click on "LED Power" button in the bottom left

### Turning on the Sound Mixer

 1. Next to the stage, there is a silver panel to the left. That is the master control panel for all audio in the building.
    - Press the rightmost button 3 times to get to the Main Menu.
    - Press the rightmost button again to open Presets.
    - Scroll with the middle wheel down to "Console Mode"
    - Press the rightmost button again to activate Console Mode.
    - Press the leftmost button 4 times to get back to the screensaver.
 2. In the booth, go to the soundboard and flip the power switch. It is on the back of the box to the upper right.

### Turning on the Spotlights

 1. Before climbing up the ladder, turn on the floor lights for upstairs. Please turn them on for your own safety

#### Turning on the Left & Right Spotlights

 1. Plug in the spotlight. There are outlets on the wall in front of the spotlights.
 2. Flip the red switch on the right side of the spotlight. It is in the middle between two other things.
 3. Wait a little bit for the light to warm up and get bright. If you want to hide it until it is ready, you can use the quick shutter.

#### Turning on the Middle Spotlight

 1. Plug in the spotlight. There are outlets on the wall in front of the spotlights.
 2. Flip the switch labelled "FAN" and wait a brief moment before proceeding.
 3. Finally, flip the switch labelled "LIGHT".

!!! danger "Middle Spotlight Warning"

    **Please** follow all the instructions regarding the fan to a T. If you do not, the bulb will, quite literally, catch fire and explode.

## Closing the Booth

### Turning off the Light Board

 1. Set the master control panel's preset to "House/Work Lights"
    - If you are on the LED Power screen, press the "Next" button twice. Don't worry about turning off LED Power as the preset will do that for you.
 2. Save your show file. The option is in `File > Save`
    - If you have forgotten if you did, please check up at the top of the screen where it says your show file name. If there is a "*" appended on, the show file needs to be saved.
    - You will lose changes you made otherwise.
 3. Go to the Browser and click on `Power Off Device`
 4. Click on the confirmation button.

!!! danger "How NOT to turn off the light board"

    Do **NOT** click the power button again.
    Do **NOT** flip the switch on the back of the console.

    These actions will damage the board, potentially irreversably.

??? question "Contradiction between these docs and the board manual"

    We are aware that the manual says that you can safely power down the machine through the power button. We were told not to as it would damage the console. While it would be cool to know if that is true/false, we do not have the luxury of finding out.

### Turning off the Sound Mixer

 1. Flip the switch on the back of the box to the upper right.
 2. Go down to the master control panel next to the stage 