# Booth Rules

In order to keep things running efficently around here, we have a myriad of rules to make sure that the whole operation can get where it needs to go, and on-time at that.

## Rule 1: No one outside of tech is allowed in the booth

This rule is for a obvious reason: There are a lot of things that can break in the booth. We don't want random people going in and screwing with the tech in there.

There are, however, obvious exceptions:

 - Faculty Members
 - Public Access TV Employees
 - Play Directors (e.g. Macuga & Class Play Coordinators)
 - Ex-Tech Members
 - Town Employees

!!! info "Tech Czar Exception"

    The current Tech Czar is allowed to invite people who are not on the list into the tech booth temporary. This can be for any number of reasons and their visit will be supervised.

## Rule 2: No food is allowed into the booth.

This rule does not have any exceptions. We want to maintain a clean environment and food would screw with that.

## Rule 3: Water is allowed but not near the equipment

This one is pretty simple: Water and electricity don't mix. Basically just hug the back wall and you should be just fine.

## Rule 4: The AC is to remain on at all times

Along with that, no student is allowed to touch the thermostat in the room. This is because we have a lot of boards in that room that suck up a lot of power. All that power creates a lot of heat as a byproduct. The AC helps to
 
 1. Cool us down whilst we are working
 2. Keep the booth from turning into a molten pit of electronicss

## Rule 5: Procedures should always be followed. Do not skip steps.

Basically, if you turn something on, please turn it off. We do not want to become Unitil's favourite customer of all time. If procedure is unclear, please visit the [Procedures](https://www.google.com/search?q=coming+soon&tbm=isch) page.