# Production Procedure Changes

We make some changes to the [Booth Procedures](../procedures) to enhance the production night experience for the audition.

 1. There are **no** lights on in the tech booth or the light loft during the production
    - If there is a desk lamp, it must be set either to or very close to the lowest brightness it possibly can go
 2. The sliding windows in the booth are to remain closed
 3. All Tech personell are to use the wireless microphone pickup boxes. They are located in a black hard case right under the security camera
    - They require the use of headphones/earbuds capable of taking input from a 3.5mm headphone jack